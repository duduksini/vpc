# vpc
[![pipeline status](https://gitlab.com/duduksini/vpc/badges/master/pipeline.svg)](https://gitlab.com/duduksini/vpc/commits/master)  

duduksini's vpc and subnets to contain Lambda and ElastiCache  

## Deployment to AWS
Gitlab CI deploys latest master branch automatically to AWS via CloudFormation template in **/aws**.

## Deployment to AWS from local machine
1. Install [awscli](https://docs.aws.amazon.com/cli/latest/userguide/installing.html), minimum version 1.14.58
1. set `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_DEFAULT_REGION`
2. In **/aws**, run `deploy.sh`
