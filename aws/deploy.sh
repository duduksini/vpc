#!/bin/bash -ex

Template=${1:-cf.yml}
StackName=${2:-duduksini-vpc}

aws cloudformation validate-template --template-body file://$Template

aws cloudformation deploy \
  --stack-name $StackName \
  --template-file $Template \
  --capabilities CAPABILITY_IAM \
  --no-fail-on-empty-changeset
